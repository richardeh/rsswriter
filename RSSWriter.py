"""
RSSWriter.py
Reads a text file, outputs RSS-compliant xml
Author: Richard Harrington
Date Created: 9/6/2013
Last Updated: 9/8/2013
"""


with open("feed.xml",'w') as outfile,open("input.txt",'r') as infile:

    # Start by writing out the xml headers
    outfile.write("<?xml version=\"1.0\"?>\n")
    outfile.write("<rss version=\"2.0\"?>\n")
    outfile.write("<channel>\n")

    # Now to read the source file and process it
    for line in infile:
        words=line.split()
        if words:
            if words[0].lower()=="title:":
                opentag, closetag="<Title>","</Title>\n"
                item="open"
            if words[0].lower()=="description:":
                opentag, closetag="<Description>","</Description>\n"
            if words[0].lower()=="link:":
                opentag,closetag="<Link>","</Link>\n"
                item="closed"

        # Prints out the items in the input.txt list
        if item=="open":
            outfile.write("<Item>\n")
            item=""
        outfile.write(opentag)
        outfile.write(" ".join(words[1:]))
        outfile.write(closetag)
        opentag,closetag="",""
        if item=="closed":
            outfile.write("</Item>\n\n")
            item=""
    outfile.write("</channel>\n")

    
